use env_logger;
use log::{error, info, trace};
use parakeet::{
    ip::{ipv6::IPv6Packet, InternetProtocolPacket},
    link::ethernet::EthernetFrame,
    pcap::{PcapGenerator, PcapLinkType, PcapPacket},
    protocol::UDPPacket,
};
use std::{
    fs::File,
    io::{self, Cursor, Write},
};

fn create_packet_in_ethernet_frame(server: bool) -> EthernetFrame {
    // udp packet
    let data = b"hello world!";
    let server_port = 80u16;
    let client_port = 23412u16;
    let udppacket = UDPPacket::new(
        if server { server_port } else { client_port },
        if server { client_port } else { server_port },
        data,
    );

    // ipv6 packet
    let common_addr_high = 0xfe80_0000_0000_0000;
    let server_addr_low = 0x0000_0000_00c0_ffee;
    let client_addr_low = 0x0080_0000_dead_beef;
    let mut ipv6packet = IPv6Packet {
        traffic_class: 0,
        flow_label: 0,
        hop_limit: 128,
        source_address_high: common_addr_high,
        source_address_low: if server {
            server_addr_low
        } else {
            client_addr_low
        },
        dest_address_high: common_addr_high,
        dest_address_low: if server {
            client_addr_low
        } else {
            server_addr_low
        },
        inner_packet: udppacket,
    };
    let (v6data, v6len) = ipv6packet.payload();

    // ethernet frame
    let server_mac: [u8; 6] = [0x02, 0x00, 0x00, 0xC0, 0xFF, 0xEE];
    let client_mac: [u8; 6] = [0x02, 0x02, 0xDE, 0xAD, 0xBE, 0xEF];
    let ethernetframe = EthernetFrame::new_from_payload(
        if server { client_mac } else { server_mac },
        if server { server_mac } else { client_mac },
        &v6data[0..v6len],
    )
    .unwrap();

    ethernetframe
}

fn main() -> std::io::Result<()> {
    env_logger::init();

    let args: Vec<String> = std::env::args().collect();

    // get output filename
    let output_fn = match args.len() {
        2 => {
            let filename = &args[1];
            info!("output file: {}", filename);
            filename
        }

        _ => {
            println!("usage: {} <filename.pcap>", &args[0]);
            std::process::exit(1);
        }
    };

    // open output file
    let mut output_file = File::create(output_fn)?;

    trace!("creating PcapGenerator");
    let mut pcapgen = PcapGenerator::new(PcapLinkType::Ethernet);

    info!("generating 5 client packets and 5 server packets");
    for i in 0..10 {
        let server = (i % 2) != 0;
        trace!(
            "packet {} (server: {})",
            i,
            if server { "yes" } else { "no" }
        );

        let etherpacket = create_packet_in_ethernet_frame(server);
        let data = {
            let (data, len) = etherpacket.to_bytes();
            Vec::from(&data[0..len])
        };

        // create and add pcap packet
        pcapgen.add_packet(PcapPacket::new(&data));
    }

    // generate pcap file
    info!("generating pcap data");
    let mut buffer = Cursor::new(Vec::new());
    let buffer = match pcapgen.write(&mut buffer) {
        Ok(()) => {
            let buf = buffer.into_inner();
            trace!("pcap buffer is {} bytes long", buf.len());

            buf
        }

        Err(e) => {
            error!("failed to get pcap data, bailing! (error: {:?}", e);
            std::process::exit(2);
        }
    };

    // save to file
    output_file.write_all(&buffer)?;
    info!("successfully wrote pcap");

    Ok(())
}
