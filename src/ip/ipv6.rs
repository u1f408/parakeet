use crate::ip::InternetProtocolPacket;
use crate::protocol::ProtocolPacket;
use crate::util::checksum;
use crate::MAX_PACKET_SIZE;

pub struct IPv6Packet<T>
where
    T: ProtocolPacket,
{
    pub traffic_class: u8,
    pub flow_label: u32,
    pub hop_limit: u8,
    pub source_address_high: u64,
    pub source_address_low: u64,
    pub dest_address_high: u64,
    pub dest_address_low: u64,
    pub inner_packet: T,
}

impl<T> IPv6Packet<T>
where
    T: ProtocolPacket,
{
    fn pseudoheader_checksum(&self) -> u16 {
        let mut c = 0;

        // source address
        c = checksum(c, &self.source_address_high.to_be_bytes());
        c = checksum(c, &self.source_address_low.to_be_bytes());
        // destination address
        c = checksum(c, &self.dest_address_high.to_be_bytes());
        c = checksum(c, &self.dest_address_low.to_be_bytes());
        // packet length
        c = checksum(c, &self.inner_packet.payload_len().to_be_bytes());
        // 24 bits (3 bytes) of zero (which we can ignore here)
        // and then the "next header" field (protocol value)
        c = checksum(c, &[T::protocol_value()]);

        c
    }
}

impl<T> InternetProtocolPacket for IPv6Packet<T>
where
    T: ProtocolPacket,
{
    fn protocol_version() -> u8 {
        6
    }

    fn payload_len(&self) -> u16 {
        // header length:
        //  - version (4 bits) + first 4 bits of traffic class (1 byte)
        //  - last 4 bits of traffic class + first 4 bits of flow label (1 byte)
        //  - next 16 bits of flow label (2 bytes)
        //  - payload length (2 bytes)
        //  - next header type (1 byte)
        //  - hop limit (1 byte)
        //  - source address (128 bits / 16 bytes)
        //  - destination address (128 bits / 16 bytes)
        let mut size = 40;

        // data length
        size += self.inner_packet.payload_len();

        size
    }

    fn payload(&mut self) -> ([u8; MAX_PACKET_SIZE], usize) {
        let mut out = [0u8; MAX_PACKET_SIZE];
        let mut idx = 0;

        macro_rules! payloadappend {
            ($e:expr) => {
                for b in $e {
                    out[idx] = *b;
                    idx += 1;
                }
            };
        }

        // version (4 bits), then first 4 bits of traffic class
        payloadappend!(&[(6 << 4) as u8 | (self.traffic_class & 0x0F) as u8]);
        // last 4 bits of traffic class, then first 4 bits of flow label
        payloadappend!(&[(self.traffic_class & 0xF0) as u8 | (self.flow_label & 0x0F) as u8]);
        // next 16 bits of flow label
        payloadappend!(&[
            ((self.flow_label & 0x0FF0) >> 4) as u8,
            ((self.flow_label & 0xF000) >> 12) as u8
        ]);
        // 16 bit payload length
        payloadappend!(&self.inner_packet.payload_len().to_be_bytes());
        // 8 bit next header
        payloadappend!(&[T::protocol_value()]);
        // 8 bit hop limit
        payloadappend!(&[self.hop_limit]);

        // source address
        payloadappend!(&self.source_address_high.to_be_bytes());
        payloadappend!(&self.source_address_low.to_be_bytes());

        // dest address
        payloadappend!(&self.dest_address_high.to_be_bytes());
        payloadappend!(&self.dest_address_low.to_be_bytes());

        // payload
        self.inner_packet
            .payload_set_transport_checksum(self.pseudoheader_checksum());
        let (pldata, pllen) = self.inner_packet.payload();
        payloadappend!(&pldata[0..pllen]);

        (out, idx)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::protocol::UDPPacket;

    fn test_packets<'a>() -> (UDPPacket<'a>, IPv6Packet<UDPPacket<'a>>) {
        let udppacket = UDPPacket::new(31423, 80, &[93u8; 32]);

        let packet = IPv6Packet {
            traffic_class: 17,
            flow_label: 35,
            hop_limit: 3,
            source_address_high: 0xfe80_0000_0000_0000,
            source_address_low: 0x0000_0000_dead_beef,
            dest_address_high: 0xfe80_0000_0000_0000,
            dest_address_low: 0x0000_0000_00c0_ffee,
            inner_packet: udppacket.clone(),
        };

        (udppacket, packet)
    }

    #[test]
    fn pseudoheader_checksum_okay() {
        let (_, packet) = test_packets();
        assert_eq!(packet.pseudoheader_checksum(), 44150);
    }

    #[test]
    fn payload_header_data_okay() {
        let (udppacket, packet) = test_packets();
        let (data, _len) = packet.payload();

        let expected = &[
            97,
            19,
            2,
            0,
            udppacket.payload_len().to_be_bytes()[0],
            udppacket.payload_len().to_be_bytes()[1],
            UDPPacket::protocol_value(),
            3,
        ];

        assert_eq!(&data[0..8], expected);
    }

    #[test]
    fn payload_header_addresses_okay() {
        let (_, packet) = test_packets();
        let (data, _len) = packet.payload();

        let expected_addr_source = [
            0xfe, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xde, 0xad,
            0xbe, 0xef,
        ];

        let expected_addr_dest = [
            0xfe, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc0,
            0xff, 0xee,
        ];

        assert_eq!(&data[8..24], expected_addr_source);
        assert_eq!(&data[24..40], expected_addr_dest);
    }
}
