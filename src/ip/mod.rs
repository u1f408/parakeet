use crate::MAX_PACKET_SIZE;
pub mod ipv6;

pub trait InternetProtocolPacket {
    fn protocol_version() -> u8;
    fn payload_len(&self) -> u16;
    fn payload(&mut self) -> ([u8; MAX_PACKET_SIZE], usize);
}
