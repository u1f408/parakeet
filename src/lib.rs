#![cfg_attr(not(feature = "std"), no_std)]
#![cfg_attr(feature = "alloc", feature(alloc_prelude))]
#![allow(dead_code, unused_imports)]

#[cfg(feature = "alloc")]
extern crate alloc;
#[cfg(feature = "alloc")]
use alloc::prelude::v1::*;

pub const MAX_PACKET_SIZE: usize = 65535;

pub mod ip;
pub mod link;
pub mod protocol;
mod util;

#[cfg(feature = "std")]
pub mod pcap;
