use std::convert::TryFrom;

pub const MAX_PAYLOAD_SIZE: usize = 1500;
pub const MAX_PAYLOAD_SIZE_U16: u16 = 1500;
// dest mac + source mac + vlan tag (optional) + length + payload + crc
pub const MAX_FRAME_SIZE: usize = 6 + 6 + 4 + 2 + MAX_PAYLOAD_SIZE + 4;

pub struct EthernetFrame {
    pub destination_mac: [u8; 6],
    pub source_mac: [u8; 6],
    pub vlan_tag: Option<[u8; 4]>,
    pub payload: [u8; MAX_PAYLOAD_SIZE],
    pub payload_length: u16,
}

impl EthernetFrame {
    pub fn new_from_payload<'a>(
        destination_mac: [u8; 6],
        source_mac: [u8; 6],
        data: &'a [u8],
    ) -> Result<EthernetFrame, &'static str> {
        if data.len() > MAX_PAYLOAD_SIZE {
            return Err("payload.len() > MAX_FRAME_SIZE");
        }

        let mut payload = [0u8; MAX_PAYLOAD_SIZE];
        for idx in 0..data.len() {
            payload[idx] = data[idx];
        }

        let mut frame = EthernetFrame {
            destination_mac,
            source_mac,
            payload,
            payload_length: u16::try_from(data.len()).unwrap_or(MAX_PAYLOAD_SIZE_U16),
            vlan_tag: None,
        };

        frame.pad_payload();
        Ok(frame)
    }

    pub fn pad_payload(&mut self) {
        let min_payload = if self.vlan_tag.is_some() {
            42usize
        } else {
            46usize
        };

        let old_length = self.payload_length as usize;
        if old_length < min_payload {
            self.payload_length = u16::try_from(min_payload).unwrap_or(46);

            for idx in old_length..min_payload {
                self.payload[idx] = 0u8;
            }
        }
    }

    pub fn to_bytes(&self) -> ([u8; MAX_FRAME_SIZE], usize) {
        let mut out = [0u8; MAX_FRAME_SIZE];
        let mut idx = 0;

        macro_rules! payloadappend {
            ($e:expr) => {
                for b in $e {
                    out[idx] = *b;
                    idx += 1;
                }
            };
        }

        payloadappend!(&self.destination_mac);
        payloadappend!(&self.source_mac);
        if self.vlan_tag.is_some() {
            payloadappend!(&self.vlan_tag.unwrap());
        }
        payloadappend!(&self.payload_length.to_be_bytes());
        payloadappend!(self.payload[0..(self.payload_length as usize)].iter());

        let framecrc = crc::crc32::checksum_ieee(&out[0..idx]);
        payloadappend!(&framecrc.to_be_bytes());

        (out, idx)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use byteorder::{BigEndian, ReadBytesExt};
    use std::io::{Cursor, Seek, SeekFrom};

    #[test]
    fn adjusts_payload_length_when_too_small() {
        let frame = EthernetFrame::new_from_payload([0u8; 6], [0u8; 6], &[1, 2, 3]).unwrap();
        assert_eq!(frame.payload_length, 46);
    }

    #[test]
    fn refuses_payload_too_big() {
        let frame = EthernetFrame::new_from_payload([0u8; 6], [0u8; 6], &([0u8; 10000][..]));
        assert!(frame.is_err());
    }

    #[test]
    fn to_bytes_makes_valid_header() {
        // construct frame
        let dest_mac: [u8; 6] = [0x02, 0x00, 0x00, 0xc0, 0xff, 0x0ee];
        let source_mac: [u8; 6] = [0x02, 0x00, 0xde, 0xad, 0xbe, 0xef];
        let data = [193u8; 128];
        let frame = EthernetFrame::new_from_payload(dest_mac, source_mac, &data).unwrap();

        // dump to bytes
        let mut rdr = {
            let (out, idx) = frame.to_bytes();
            Cursor::new(out[0..idx].to_vec())
        };

        // check destination mac
        for octet in &dest_mac {
            assert_eq!(rdr.read_u8().unwrap(), *octet);
        }

        // check source mac
        for octet in &source_mac {
            assert_eq!(rdr.read_u8().unwrap(), *octet);
        }

        // check payload length
        assert_eq!(rdr.read_u16::<BigEndian>().unwrap(), 128);
    }

    #[test]
    fn to_bytes_makes_valid_frame_crc() {
        // construct frame
        let dest_mac: [u8; 6] = [0x02, 0x00, 0x00, 0xc0, 0xff, 0x0ee];
        let source_mac: [u8; 6] = [0x02, 0x00, 0xde, 0xad, 0xbe, 0xef];
        let data = [193u8; 128];
        let frame = EthernetFrame::new_from_payload(dest_mac, source_mac, &data).unwrap();

        // dump to bytes
        let mut rdr = {
            let (out, idx) = frame.to_bytes();
            Cursor::new(out[0..idx].to_vec())
        };

        // seek from end back 4 bytes
        rdr.seek(SeekFrom::End(-4)).unwrap();

        // read CRC
        let crc = rdr.read_u32::<BigEndian>().unwrap();
        assert_eq!(crc, 0xD22725FA);
    }
}
