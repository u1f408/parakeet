/// The magic number in the header of a Pcap file
pub const MAGIC_NUMBER: u32 = 0xA1B2C3D4;

/// The "major" version number of the Pcap file format that we can create
pub const VERSION_MAJOR: u16 = 2;

/// The "minor" version number of the Pcap file format that we can create
pub const VERSION_MINOR: u16 = 4;

/// The "snapshot length" for packet captures. This is the maximum length of
/// any one packet.
pub const SNAP_LENGTH: u32 = 65535;

/// Link-layer header types.
///
/// See [the Link-Layer Header Types](https://www.tcpdump.org/linktypes.html)
/// page on the tcpdump website for information about these.
#[derive(Clone, Copy, Debug)]
pub enum PcapLinkType {
    /// BSD loopback encapsulation
    NullType = 0,

    /// IEEE 802.3 Ethernet
    Ethernet = 1,

    /// IEEE 802.15.4 Low-Rate Wireless Network
    ///
    /// Packets of this link type include the FCS at the end of each frame.
    LowRateWireless802_15_4 = 195,
}
