use std::io::{self, Seek, Write};

mod packet;
pub use self::packet::PcapPacket;
mod constants;
pub use self::constants::*;
#[cfg(test)]
mod tests;

/// A simple generator for Pcap files.
#[derive(Clone)]
pub struct PcapGenerator {
    /// The link type for this Pcap file
    pub link_type: PcapLinkType,

    /// The offset from GMT in seconds for timestamps in packet headers.
    /// This should generally be set to zero.
    pub this_zone: i32,

    /// Have we written the pcap header?
    have_written_pcap_header: bool,

    /// Packets still yet to be written to the Pcap file
    packets_to_write: Vec<PcapPacket>,
}

impl PcapGenerator {
    pub fn new(link_type: PcapLinkType) -> PcapGenerator {
        PcapGenerator {
            link_type,
            this_zone: 0,
            have_written_pcap_header: false,
            packets_to_write: Vec::new(),
        }
    }

    pub fn add_packet(&mut self, packet: PcapPacket) {
        self.packets_to_write.push(packet);
    }

    pub fn write<W: Write + Seek>(&mut self, writer: &mut W) -> io::Result<()> {
        if !self.have_written_pcap_header {
            writer.write(&MAGIC_NUMBER.to_be_bytes())?;
            writer.write(&VERSION_MAJOR.to_be_bytes())?;
            writer.write(&VERSION_MINOR.to_be_bytes())?;
            writer.write(&self.this_zone.to_be_bytes())?;
            writer.write(&(0u32).to_be_bytes())?;
            writer.write(&SNAP_LENGTH.to_be_bytes())?;
            writer.write(&(self.link_type as u32).to_be_bytes())?;

            self.have_written_pcap_header = true;
        }

        loop {
            match self.packets_to_write.pop() {
                Some(packet) => {
                    writer.write(&packet.to_bytes()?)?;
                }

                None => {
                    break;
                }
            }
        }

        Ok(())
    }
}
