use crate::pcap::SNAP_LENGTH;
use std::convert::TryFrom;
use std::io::{self, Cursor, Write};
use std::time::SystemTime;

/// The size of the serialised pcap packet header
// ts_sec: u32, ts_usec: u32, incl_len: u32, orig_len: u32
pub const HEADER_SIZE: usize = 16;

/// Representation of a network packet that can be serialised to a pcap file
#[derive(Clone)]
pub struct PcapPacket {
    pub ts_sec: u32,
    pub ts_usec: u32,
    pub data: Vec<u8>,
}

impl PcapPacket {
    /// Create a PcapPacket with the given data and time information.
    pub fn new_with_time<'a>(ts_sec: u32, ts_usec: u32, data: &'a [u8]) -> PcapPacket {
        PcapPacket {
            ts_sec,
            ts_usec,
            data: Vec::from(data),
        }
    }

    /// Create a PcapPacket with the given data, using the current system time
    /// as the packet timestamp.
    pub fn new<'a>(data: &'a [u8]) -> PcapPacket {
        let (sec, msec) = match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
            Ok(n) => (u32::try_from(n.as_secs()).unwrap(), n.subsec_micros()),
            Err(_) => (0, 0),
        };

        PcapPacket::new_with_time(sec, msec, data)
    }

    /// Returns the (possibly truncated) length of this packet's data.
    /// If the length of the data of this PcapPacket is less than
    /// `SNAP_LENGTH - HEADER_SIZE` (the maximum size), the original length is
    /// used. If the length of the data of this PcapPacket is greater than the
    /// maximum size, the maximum size is used instead.
    fn data_trunclen(&self) -> usize {
        let snap_length = SNAP_LENGTH as usize;
        if self.data.len() < snap_length - HEADER_SIZE {
            self.data.len()
        } else {
            snap_length - HEADER_SIZE
        }
    }

    /// Returns this packet, including the pcap header information, as a Vec<u8>.
    pub fn to_bytes(&self) -> io::Result<Vec<u8>> {
        let mut out = Cursor::new(Vec::new());

        // write time part of header
        out.write(&self.ts_sec.to_be_bytes())?;
        out.write(&self.ts_usec.to_be_bytes())?;
        // write the "included length" (as from `self.data_trunclen()`)
        out.write(&(u32::try_from(self.data_trunclen()).unwrap().to_be_bytes()))?;
        // write the actual packet length
        out.write(&(u32::try_from(self.data.len()).unwrap().to_be_bytes()))?;
        // write the payload
        out.write(&self.data)?;

        Ok(out.into_inner())
    }
}
