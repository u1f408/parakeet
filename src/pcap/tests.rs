use super::*;
use byteorder::{BigEndian, ReadBytesExt};
use std::io::{self, Cursor, Seek, SeekFrom, Write};

#[test]
fn packet_to_bytes_okay() {
    let packet = PcapPacket::new_with_time(1578987400, 0, &[1, 2, 3, 4, 5]);
    let data = packet.to_bytes().unwrap();
    let expected = &[
        0x5e, 0x1d, 0x6f, 0x88, // time (sec)
        0x00, 0x00, 0x00, 0x00, // time (usec)
        0x00, 0x00, 0x00, 0x05, // included length
        0x00, 0x00, 0x00, 0x05, // actual length
        0x01, 0x02, 0x03, 0x04, 0x05, // packet data
    ];

    assert_eq!(data, expected);
}

#[test]
fn generator_writes_header() {
    let mut gen = PcapGenerator::new(PcapLinkType::Ethernet);
    let mut out = Cursor::new(Vec::new());

    // write header
    gen.write(&mut out).unwrap();

    // rewind
    out.set_position(0);

    // check magic number
    assert_eq!(out.read_u32::<BigEndian>().unwrap(), MAGIC_NUMBER);

    // check major/minor versions
    assert_eq!(out.read_u16::<BigEndian>().unwrap(), VERSION_MAJOR);
    assert_eq!(out.read_u16::<BigEndian>().unwrap(), VERSION_MINOR);

    // skip forward over three 32bit values
    out.seek(SeekFrom::Current(12)).unwrap();

    // check link type
    assert_eq!(
        out.read_u32::<BigEndian>().unwrap(),
        PcapLinkType::Ethernet as u32
    );
}

#[test]
fn generator_writes_valid_file_with_a_packet() {
    let mut gen = PcapGenerator::new(PcapLinkType::Ethernet);
    let packet = PcapPacket::new_with_time(1578987400, 0, &[1, 2, 3, 4, 5]);
    gen.add_packet(packet);

    // write
    let mut out = Cursor::new(Vec::new());
    gen.write(&mut out).unwrap();
    let out = out.into_inner();

    // check file header
    let expected_file_header = &[
        0xa1, 0xb2, 0xc3, 0xd4, // magic number,
        0x00, 0x02, 0x00, 0x04, // major/minor version
        0x00, 0x00, 0x00, 0x00, // this zone
        0x00, 0x00, 0x00, 0x00, // sigfigs
        0x00, 0x00, 0xFF, 0xFF, // snap len
        0x00, 0x00, 0x00, 0x01, // data link type (ethernet)
    ];
    assert_eq!(&out[0..(expected_file_header.len())], expected_file_header);

    // check packet header
    let expected_packet_header = &[
        0x5e, 0x1d, 0x6f, 0x88, // time (sec)
        0x00, 0x00, 0x00, 0x00, // time (usec)
        0x00, 0x00, 0x00, 0x05, // included length
        0x00, 0x00, 0x00, 0x05, // actual length
    ];
    assert_eq!(
        &out[(expected_file_header.len())
            ..(expected_file_header.len() + expected_packet_header.len())],
        expected_packet_header
    );

    let expected_packet_data = &[0x01, 0x02, 0x03, 0x04, 0x05];
    assert_eq!(
        &out[(expected_file_header.len() + expected_packet_header.len())
            ..(expected_file_header.len()
                + expected_packet_header.len()
                + expected_packet_data.len())],
        expected_packet_data
    );
}
