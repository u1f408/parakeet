use crate::MAX_PACKET_SIZE;
mod udp;
pub use self::udp::UDPPacket;

pub trait ProtocolPacket {
    fn protocol_value() -> u8;
    fn payload_len(&self) -> u16;
    fn payload_set_transport_checksum(&mut self, chksum: u16);
    fn payload_calc_checksum(&mut self) -> u16;
    fn payload(&self) -> ([u8; MAX_PACKET_SIZE], usize);
}
