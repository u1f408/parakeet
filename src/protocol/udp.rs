use crate::protocol::ProtocolPacket;
use crate::util::checksum;
use crate::MAX_PACKET_SIZE;
use core::convert::TryFrom;

#[derive(Clone, Debug)]
pub struct UDPPacket<'a> {
    pub source_port: u16,
    pub dest_port: u16,
    pub data: &'a [u8],
    transport_checksum: u16,
    checksum: u16,
}

impl<'a> UDPPacket<'a> {
    pub fn new(source_port: u16, dest_port: u16, data: &'a [u8]) -> UDPPacket {
        let mut packet = UDPPacket {
            source_port,
            dest_port,
            data,
            transport_checksum: 0,
            checksum: 0,
        };

        packet.payload_calc_checksum();
        packet
    }

    fn inner_checksum(&self) -> u16 {
        let mut c = 0;
        c = checksum(c, &self.source_port.to_be_bytes());
        c = checksum(c, &self.dest_port.to_be_bytes());
        c = checksum(c, &self.payload_len().to_be_bytes());
        c = checksum(c, &self.data);

        c
    }
}

impl<'a> ProtocolPacket for UDPPacket<'a> {
    fn protocol_value() -> u8 {
        17
    }

    fn payload_len(&self) -> u16 {
        // header length:
        //  - source port (2 bytes)
        //  - dest port (2 bytes)
        //  - payload length (2 bytes)
        //  - checksum (2 bytes)
        let mut size = 8;

        // data length
        size += u16::try_from(self.data.len()).unwrap_or(0);

        size
    }

    fn payload_set_transport_checksum(&mut self, new: u16) {
        self.transport_checksum = new;
        self.payload_calc_checksum();
    }

    fn payload_calc_checksum(&mut self) -> u16 {
        let mut sum = self.inner_checksum();
        if self.transport_checksum > 0 {
            sum = checksum(sum, &self.transport_checksum.to_be_bytes());
        }

        self.checksum = sum;
        sum
    }

    fn payload(&self) -> ([u8; MAX_PACKET_SIZE], usize) {
        let mut out = [0u8; MAX_PACKET_SIZE];
        let mut idx = 0;

        macro_rules! payloadappend {
            ($e:expr) => {
                for b in $e {
                    out[idx] = *b;
                    idx += 1;
                }
            };
        }

        payloadappend!(&self.source_port.to_be_bytes());
        payloadappend!(&self.dest_port.to_be_bytes());
        payloadappend!(&self.payload_len().to_be_bytes());
        payloadappend!(&self.checksum.to_be_bytes());
        payloadappend!(self.data.iter());

        (out, idx)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use byteorder::{BigEndian, ReadBytesExt};
    use std::io::Cursor;

    #[test]
    fn inner_checksum_okay() {
        let packet = UDPPacket::new(31423, 80, &[93u8; 32]);
        assert_eq!(packet.inner_checksum(), 20749);
    }

    #[test]
    fn payload_format_okay() {
        let packet = UDPPacket::new(31423, 80, &[93u8; 32]);
        let mut rdr = {
            let (data, len) = packet.payload();
            Cursor::new(data[0..len].to_vec())
        };

        // check source port
        assert_eq!(rdr.read_u16::<BigEndian>().unwrap(), 31423);
        // check dest port
        assert_eq!(rdr.read_u16::<BigEndian>().unwrap(), 80);
        // check payload length -- (udp header + data size) = (8 + 32) = 40
        assert_eq!(rdr.read_u16::<BigEndian>().unwrap(), 40);
    }

    #[test]
    fn payload_checksum_okay() {
        let mut packet = UDPPacket::new(31423, 80, &[93u8; 32]);
        // set the payload checksum to 12345 for this test
        packet.payload_set_transport_checksum(0xF3B0);

        // get payload data
        let mut rdr = {
            let (data, len) = packet.payload();
            Cursor::new(data[0..len].to_vec())
        };

        // ignore first three u16s
        rdr.set_position(6);

        // get checksum
        let packet_checksum = rdr.read_u16::<BigEndian>().unwrap();
        assert_eq!(packet_checksum, 17598);
    }
}
