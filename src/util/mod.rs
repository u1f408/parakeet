pub fn checksum(mut sum: u16, data: &[u8]) -> u16 {
    for d in data.chunks(2) {
        let one = d[0];
        let two = if d.len() == 2 { d[1] } else { 0 };

        let t = ((one as u16) << 8) + two as u16;
        sum = sum.wrapping_add(t);
        if sum < t {
            sum = sum.wrapping_add(1);
        }
    }

    sum
}

#[cfg(test)]
mod checksum_tests {
    use super::*;

    #[test]
    fn test_basic() {
        assert_eq!(checksum(0, &[0u8; 16]), 0);
        assert_eq!(checksum(0, &[255u8, 255u8, 0u8]), 65535);
        assert_eq!(checksum(20749, &[0xF3u8, 0xB0u8]), 17598);
    }

    #[test]
    fn test_wrapping() {
        assert_eq!(checksum(32, &[255u8, 255u8, 32u8, 0u8]), 8192 + 32);
    }
}
